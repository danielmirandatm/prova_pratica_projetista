/**
 * @brief Programa criado para leitura e apresentacao de tag em arquivo xml [Q3]
 * @author Daniel Miranda <danielmiranda@mobitbrasil.com.br>
 * @date 2020-09-10
**/

#include <iostream>
#include <thread>
#include <mutex>
#include <queue>
#include "pugixml.hpp"

using namespace pugi;
using namespace std;

xml_document xml_doc;
mutex mtx;


/**
* @brief thread para producao da fila com as tags do arquivo xml
* @param Queue  Ponteiro para a fila
**/
static void thread_producer(queue<xpath_node> *Queue)
{
    // Aquisicao do mutex
    mtx.lock();

    // Nó raiz
    xpath_node_set Msg = xml_doc.select_nodes("Mensagem");

    // Enquanto houver tags dentro da raiz "Mensagem"
    for (xpath_node_set::const_iterator i = Msg.begin(); i != Msg.end(); ++i)
	{
    	xpath_node tag = *i;

        // Insere item na lista
		Queue->push(tag);
	}

    // Libera o mutex
    mtx.unlock();
}


/**
* @brief thread para consumo da fila em busca do valor da tag "payload"
* @param Queue  Ponteiro para a fila
**/
static void thread_consumer(queue<xpath_node> *Queue)
{
    // Aquisicao do mutex
    mtx.lock();

    while(!Queue->empty())
    {

        // Pega primeiro item da lista
    	xpath_node first_node = Queue->front();

        // Libera elemento
    	Queue->pop();

        // Tag procurada (payload)
    	xpath_node wanted_tag  = first_node.node().select_node("payload");

        // Se achou tag "payload"
    	if(wanted_tag)
        {
            // Printa valor da tag "payload"
    		cout << wanted_tag.node().child_value() << "\n";
    	}
    }

    // Libera o mutex
    mtx.unlock();
}


// Funcao principal
int main()
{
    // Le arquivo xml
    xml_doc.load_file("input.xml");

    // Cria fila
    queue<xpath_node> queue;

    // Inicializa threads
    thread thread_p(thread_producer, &queue);
    thread thread_c(thread_consumer, &queue);

    // Finaliza threads
    thread_p.join();
    thread_c.join();
}
