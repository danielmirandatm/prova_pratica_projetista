#include <iostream>
#include <vector>
#include <thread>
#include <mutex>
#include <syslog.h>

static const int THREAD_COUNT = 10;
std::mutex mtx;

static void print_log(int id)
{
    // Aquisicao de mutex para evitar concorrencia entre as threads
    mtx.lock();

    // Seta a prioridade para todos os niveis acima de LOG_INFO
    setlogmask(LOG_UPTO (LOG_INFO));

    // Configura os parametros de log:
    // Nome do processo "q2"
    // Logar no console em caso de falha na escrita do syslog
    // Logar o PID do process
    // Nao utilizar delay na escrita do log
    openlog("q2", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);

    syslog(LOG_INFO, "--------------------");
    syslog(LOG_INFO, "Iniciando bloco %d", id);
    syslog(LOG_INFO, "Hello world from thread %d", id);
    syslog(LOG_INFO, "Fim do bloco %d", id);
    syslog(LOG_INFO, "--------------------");

    // Fecha o log
    closelog();
    
    // Liberacao de mutex
    mtx.unlock();
}



int main()
{
    std::vector<std::thread> v;

    for (size_t i = 0; i < THREAD_COUNT; i++)
    {
        v.emplace_back(print_log, i);
    }

    for (auto &t : v)
    {
        t.join();
    }

    return 0;
}