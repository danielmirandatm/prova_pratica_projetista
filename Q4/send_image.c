/**
 * @brief Programa criado para leitura e envio de uma imagem por socket unix
 * @author Daniel Miranda <danielmiranda@mobitbrasil.com.br>
 * @date 2020-09-11
**/

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define CHANNEL_NUM 3

// Estrutura da imagem
typedef struct
{
	// ponteiro para a imagem
	uint8_t *image;
	// largura da image
	int width;
	// altura da imagem
	int height;
	// bits por pixel da imagem
	int bpp;
} Image;


/**
 * @brief Funcao para escrita da imagem no socket unix
 * 
 * @param socket_conf Configuracoes do socket unix
 * @param image_length Tamanho da imagem
 * @param picture Ponteiro para a imagem
 * @param length Tamanho de memoria alocado para a imagem
**/
void write_image(int socket_conf, int image_length, Image *picture, int length)
{
	int length_int = sizeof(int);
	write(socket_conf, &length_int, sizeof(length_int));
	
	// Escreve tamanho da imagem no socket
	write(socket_conf, &image_length, length_int);
	// Escreve imagem no socket
	write(socket_conf, picture, length);
}


int main (int argc, char* const argv[])
{
    // Socket unix utilizado para troca de mensagens
	const char* const socket_file = "/tmp/socket_unix";
	struct sockaddr_un socket_name;
	Image picture;
	int socket_conf;
	int image_length;

    // Carrega a imagem e as informacoes de largura, altura e bits por pixel
	picture.image = stbi_load("image_test.png", &picture.width, &picture.height, &picture.bpp, CHANNEL_NUM);
    // Calcula o tamanho da imagem
	image_length = picture.width * picture.height * CHANNEL_NUM;

	// Configuracoes do socket
	socket_conf = socket(PF_LOCAL, SOCK_STREAM, 0);
	socket_name.sun_family = AF_LOCAL;
	strcpy(socket_name.sun_path, socket_file);
	connect(socket_conf, (sockaddr *)&socket_name, SUN_LEN(&socket_name));

	// Escreve imagem no socket
	write_image(socket_conf, image_length, &picture, sizeof(picture));

	// Fecha o socket
	close(socket_conf);

	return 0;
}