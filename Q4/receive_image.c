/**
 * @brief Programa criado para recbimento de imagem via socket unix
 * @author Daniel Miranda <danielmiranda@mobitbrasil.com.br>
 * @date 2020-09-11
**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#define CHANNEL_NUM 3

// Estrutura da imagem
typedef struct
{
	// ponteiro para a imagem
	uint8_t *image;
	// largura da image
	int width;
	// altura da imagem
	int height;
	// bits por pixel da imagem
	int bpp;
} Image;

/**
 * @brief Funcao para leitura da imagem por socket unix 
 * 
 * @param receive_image_conf Configuracao do socket unix
 * @return 1 se encontrar imagem no socket, caso contrario retorna 0
**/
int receive_image_length(int receive_image_conf)
{
	while(1){
		int length;
		int *picture_length;
		Image * picture;

		if(read(receive_image_conf, &length, sizeof(length)) == 0){
			return 0;
		}

        // Aloca memoria para receber tamanho da imagem
		picture_length = (int *)malloc(length);

        // Le tamanho da imagem
		read(receive_image_conf, picture_length, length);

        // Aloca memoria para receber imagem
		picture = (Image *)malloc(length);

        // Le imagem
		read(receive_image_conf, picture, length);

		// Salva imagem no diretorio "/var"
		stbi_write_png("/var/", picture->width, picture->height, CHANNEL_NUM, picture->image, picture->width*CHANNEL_NUM);

        // libera memoria alocada para tamanho da imagem
		free(picture_length);
		return 1;
	}
}


int main(int argc, char* const argv[])
{
    // Socket unix utilizado para troca de mensagens
	const char* const socket_file = "/tmp/socket_unix";
    struct sockaddr_un socket_name;
	int socket_conf;
	int quit_msg;

    // Configuracoes do socket
	socket_conf = socket(PF_LOCAL, SOCK_STREAM, 0);
	socket_name.sun_family = AF_LOCAL;
	strcpy(socket_name.sun_path, socket_file);
	bind(socket_conf, (sockaddr *)&socket_name, SUN_LEN(&socket_name));
	listen(socket_conf, 5);

    // Enquanto nao receber imagem, fica no loop procurando a imagem no socket
	do
    {
		struct sockaddr_un receive_image;
		socklen_t receive_image_len;
		int receive_image_conf;

		receive_image_conf = accept(socket_conf, (sockaddr *)&receive_image, &receive_image_len);
		quit_msg = receive_image_length(receive_image_conf);
		close(receive_image_conf);

	}   while(!quit_msg);

	// Fecha o socket
	close(socket_conf);

    // Tenta remover o arquivo socket
	if (unlink(socket_file) != 0) {
		return EXIT_FAILURE;
	}

	return 0;
}
