#!/bin/bash
# ---
# Script que verifica a porcentagem de utilização de todas as partições, 
# o uso de memória RAM e a temperatura dos cores do sistema e 
# envia um e-mail de alerta para cada variável monitorada que estiver acima dos limites estabelecidos 
# ---
# Author: Daniel Miranda <danielmirandatm@gmail.com>
# Date: 2020-09-10


# CONSTANTES ===

HOSTNAME=`hostname`

# Arquivo de log do script
LOG_FILE='/var/log/script_monitor.log'

# Arquivo temporario para escrita e leitura do comando 'df'
PARTITION_FILE='/tmp/partition.txt'
# Arquivo temporario para escrita e leitura do comando 'sensor'
CORE_TEMPERATURE_FILE='/tmp/core_temperature.txt'

# Arquivo temporario para escrita e leitura dos alertas de hd
HD_ALERT_FILE='/tmp/hd_alert_file.txt'
# Arquivo temporario para escrita e leitura dos alertas de memoria RAM
RAM_ALERT_FILE='/tmp/ram_alert_file.txt'
# Arquivo temporario para escrita e leitura dos alertas de CPU
CPU_ALERT_FILE='/tmp/cpu_alert_file.txt'

# Limite de uso das particoes do HD (%)
MAX_USE_HD=70
# Limite de uso da memoria RAM (%)
MAX_USE_RAM=70
# Limite de temperatura dos cores da CPU (°C)
MAX_TEMP_CPU=70

# ===

# Funcao para pegar data e hora atuais
get_date()
{
  date=`date +'%Y-%m-%d %H:%M:%S'`
}


# Funcao utilizada para verificacao das particoes do HD
partition_verify()
{
  # Lista particoes do HD e escreve no arquivo temporario
  df -h | grep '/dev' > $PARTITION_FILE
  
  # Verifica quantidade de particoes
  num_partition=`cat $PARTITION_FILE | wc -l`

  for (( i=1;i<=$num_partition;i++ ));
  do
    line=`cat $PARTITION_FILE | head -n $i | tail -n 1`
    # Pega identificacao da particao
    partition=`echo $line | awk -F ' ' '{print($1)}'`
    # Pega espaco utilizado pela particao
    used=`echo $line | awk -F ' ' '{print($5)}' | cut -f1 -d '%'`
    # Pega diretorio montado na particao
    dir=`echo $line | awk -F ' ' '{print($6)}'`
    
    if [ $used -gt $MAX_USE_HD ];
    then
      get_date
      # Escreve saida de alerta no log e no arquivo temporario de alertas do HD
      echo "$date [$HOSTNAME] [ALERTA] - Particao $partition (diretorio $dir) chegou a $used de utilizacao do disco!" | tee -a $LOG_FILE $HD_ALERT_FILE >/dev/null
    fi
  done

  rm -f $PARTITION_FILE
  
  if [ -f $HD_ALERT_FILE ];
  then
    cat $HD_ALERT_FILE | mail -s 'ALERTA DE PARTICAO CHEIA' danielmiranda@mobitbrasil.com.br
    rm -f $HD_ALERT_FILE
  fi
}


# Funcao utlizaca para verificacao da utlizacao de memoria RAM
ram_verify()
{
  # Le informacoes da memoria
  mem=`free | grep Mem`
  # Memoria total
  total=`echo $mem | awk -F ' ' '{print($2)}'`
  # Memoria utilizada
  used=`echo $mem | awk -F ' ' '{print($3)}'`
  # Memoria utilizada (%)
  used_percent=`echo $(( $used * 100 / $total ))`

  if [ $used_percent -gt $MAX_USE_RAM ];
    then
      get_date
      # Escreve saida de alerta no log e no arquivo temporario de alertas da RAM
      echo "$date [$HOSTNAME] [ALERTA] - Memoria RAM com $used_percent% de utilizacao!" | tee -a $LOG_FILE $RAM_ALERT_FILE >/dev/null
  fi

  if [ -f $RAM_ALERT_FILE ];
  then
    cat $RAM_ALERT_FILE | mail -s 'ALERTA DE UTILIZACAO DE MEMORIA RAM' danielmiranda@mobitbrasil.com.br
    rm -f $RAM_ALERT_FILE
  fi
}


# Funcao utilizada para verificar a temperatura dos cores da CPU
core_temperature_verify()
{
  # Lista de cores da CPU e suas temperaturas
  sensors | grep Core > $CORE_TEMPERATURE_FILE

  # Verifica quantidade de cores
  num_cores=`cat $CORE_TEMPERATURE_FILE | wc -l`

  for (( i=1;i<=$num_cores;i++ ));
  do
    line=`cat $CORE_TEMPERATURE_FILE | head -n $i | tail -n 1`
    # Pega ID do core da CPU
    core_id=`echo $line | cut -f1 -d ":"`
    # Pega temperatura do core da CPU
    core_temperature=`echo $line | cut -f2 -d "+" | cut -f1 -d "."`

    if [ $core_temperature -gt $MAX_TEMP_CPU ];
    then
      get_date
      # Escreve saida de alerta no log e no arquivo temporario de alertas da CPU
      echo "$date [$HOSTNAME] [ALERTA] - $core_id da CPU com temperatura $core_temperature°C!" | tee -a $LOG_FILE $CPU_ALERT_FILE >/dev/null
    fi
  done

  rm -f $CORE_TEMPERATURE_FILE

  if [ -f $CPU_ALERT_FILE ];
  then
    cat $CPU_ALERT_FILE | mail -s 'ALERTA DE TEMPERATURA DE CORE DA CPU' danielmiranda@mobitbrasil.com.br
    rm -f $CPU_ALERT_FILE
  fi
}


# Funcao Principal
main()
{
  # Executa as funcoes de verificacao
  partition_verify
  ram_verify
  core_temperature_verify
}

main