#!/bin/bash

PWD=`pwd`
HOSTNAME=`hostname`

apt update
apt install postfix mailutils lm-sensors

echo -e "\n\n"
read -p "Digite a conta do gmail que sera utilizado pelo $HOSTNAME: " gmail
read -sp "Digite a senha desta conta: " pass


echo "Configurando serviços de e-mail.."
echo "[smtp.gmail.com]:587    $gmail:$pass" > /etc/postfix/sasl_passwd
chmod 600 /etc/postfix/sasl_passwd

cat /etc/postfix | grep "smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd" > /dev/null
if [ $? -ne 0 ];
then
    echo -e "relayhost = [smtp.gmail.com]:587\nsmtp_use_tls = yes\nsmtp_sasl_auth_enable = yes\nsmtp_sasl_security_options =\nsmtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd\nsmtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt" >> /etc/postfix/main.cf
fi

postmap /etc/postfix/sasl_passwd
systemctl restart postfix.service


echo "Configurando script_monitor..."
cp $PWD/script_monitor.sh /usr/sbin/script_monitor.sh

cat /etc/crontab | grep "*/5 * * * * root bash /usr/sbin/script_monitor.sh" > /dev/null
if [ $? -ne 0 ];
then
    echo "*/5 * * * * root bash /usr/sbin/script_monitor.sh" >> /etc/crontab
fi

echo "Reiniciando serviço cron"
service cron stop
service cron start