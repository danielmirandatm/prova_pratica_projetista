# README

## Descricao
Repositorio criado para resolucao das questoes para a vaga de projetista de sistemas embarcados no setor de P&D da empresa Mobit.

### Candidato:
    Daniel Miranda T. Mendes

### Email:
    danielmirandatm@gmail.com
    danielmiranda@mobitbrasil.com.br

## Questao 1
Para realizar a instalacao das dependencias e configuracao do script monitor, o usuario deve acessar o diretorio da [Questao 1](Q1) e em seguida executar o seguinte comando:
    
    $ sudo bash install_script_monitor.sh

## Questao 2
Para realizar a compilacao do programa da questao 2, o usuario deve executar:

    $ make

Para executar o progama:

    $ ./q2

Para verificar a saida no log, deve executar:

    $ tail -n 1000 -f /var/log/syslog


Para excluir o binario gerado, deve executar:

    $ make clean

## Questao 3
Para realizar a compilacao do programa da questao 3, o usuario deve executar:

    $ make

Para executar o progama:

    $ ./q3

Para excluir o binario gerado, deve executar:

    $ make clean

## Questao 4
Para realizar a compilacao do programa da questao 4, o usuario deve executar:

    $ make

Para executar o progama:

    $ ./send_image
    $ ./receive_image

Para excluir o binario gerado, deve executar:

    $ make clean